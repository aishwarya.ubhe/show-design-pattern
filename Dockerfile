FROM node:latest

MAINTAINER kajal <kajal.gaikwad@wgblindia.com>

WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN yarn install
RUN npm install

ADD src /usr/src/app/src
ADD public /usr/src/app/public

RUN npm build

CMD [ "npm", "start" ]

