import {
  FETCH_SUMMARY,
  FETCH_SUMMARY_SUCCESS,
  FETCH_SUMMARY_FAILURE
} from './summaryTypes'

const initialState = {
  loading: false,
  shows: [],
  error: ''
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUMMARY:
      return {
        ...state,
        loading: true
      }
    case FETCH_SUMMARY_SUCCESS:
      return {
        loading: false,
        shows: action.payload,
        error: ''
      }
    case FETCH_SUMMARY_FAILURE:
      return {
        loading: false,
        shows: [],
        error: action.payload
      }
    default: return state
  }
}

export default reducer
