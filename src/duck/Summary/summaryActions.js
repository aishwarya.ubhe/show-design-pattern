import axios from 'axios'
import {
  FETCH_SUMMARY,
  FETCH_SUMMARY_SUCCESS,
  FETCH_SUMMARY_FAILURE
} from './summaryTypes'

export const fetchSummary = () => {
  return (dispatch) => {
    dispatch(fetchSummary())
    axios
      .get('http://api.tvmaze.com/shows/${match.params.id}')
      .then(response => {
        const shows = response.data
        dispatch(fetchShowsSuccess(shows))
      })
      .catch(error => {
        dispatch(fetchShowsFailure(error.message))
      })
  }
}

export const fetchSummary = () => {
  return {
    type: FETCH_SHOWS
  }
}

export const fetchSummarySuccess = shows => {
  return {
    type: FETCH_SUMMARY_SUCCESS,
    payload: shows.id
  }
}

export const fetchSummaryFailure = error => {
  return {
    type: FETCH_SUMMARY_FAILURE,
    payload: error
  }
}
