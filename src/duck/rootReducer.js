import { combineReducers } from 'redux'
import reducer from './Home/reducer'
import summaryReducer from './Summary/summaryReducer'

const rootReducer = combineReducers({
  show: reducer,
  summary: summaryReducer
})

export default rootReducer
