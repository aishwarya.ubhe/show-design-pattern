import axios from 'axios'
import {
  FETCH_SHOWS,
  FETCH_SHOWS_SUCCESS,
  FETCH_SHOWS_FAILURE
} from './types'

export const fetch = () => {
  return (dispatch) => {
    dispatch(fetchShows())
    axios
      .get('http://api.tvmaze.com/shows')
      .then(response => {
        const shows = response.data
        dispatch(fetchShowsSuccess(shows))
      })
      .catch(error => {
        dispatch(fetchShowsFailure(error.message))
      })
  }
}

export const fetchShows = () => {
  return {
    type: FETCH_SHOWS
  }
}

export const fetchShowsSuccess = shows => {
  return {
    type: FETCH_SHOWS_SUCCESS,
    payload: shows
  }
}

export const fetchShowsFailure = error => {
  return {
    type: FETCH_SHOWS_FAILURE,
    payload: error
  }
}
