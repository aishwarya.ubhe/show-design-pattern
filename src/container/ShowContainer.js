import Show from '../components/Show'
import React from 'react'
import { connect } from 'react-redux'
import { fetchSummary } from '../duck/Summary/summaryActions'

const mapStateToProps = state => {
  return {
    showData: state.shows
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSummary: () => dispatch(fetchSummary())
  }
}

const ShowContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Show)
export default ShowContainer