import Home from '../components/Home'
import React from 'react'
import { connect } from 'react-redux'
import { fetch } from '../duck/Home/actions'

const mapStateToProps = state => {
  return {
    showData: state.show
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetch: () => dispatch(fetch())
  }
}

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
export default HomeContainer