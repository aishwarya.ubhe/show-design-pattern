import ShowSummary from '../components/ShowSummary'
import React from 'react'
import { connect } from 'react-redux'
import { fetchSummary } from '../duck/Summary/summaryActions'

const mapStateToProps = state => {
  return {
    showData: state.shows
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSummary: () => dispatch(fetchSummary())
  }
}

const ShowSummaryContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowSummary)
export default ShowSummaryContainer