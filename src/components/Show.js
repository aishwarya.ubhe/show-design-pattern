import React, { useEffect} from 'react'
import {Link} from 'react-router-dom'

function Show ({ showList, fetchShowList }) {
  useEffect(() => {
    fetch()
  }, [])

  return (
    <div>
      {shows.map(s => (
        <div key={s.id}>
        <Link to={`/show/${s.id}`}>{s.name}</Link>
        </div>
        ))}
    </div>
  )
}

export default Show
