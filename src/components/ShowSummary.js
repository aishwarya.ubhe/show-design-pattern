import React, {useEffect} from 'react';

function ShowSummary({showSummaryData, fetchSummary, match }) {
  useEffect(() => {
    fetchSummary();
    console.log(match);
  },[])

  // const [show, setShow] = useState({});
  // const fetchShow = async () => {
  //   const data = await fetch(`http://api.tvmaze.com/shows/${match.params.id}`);
  //   const show = await data.json();
  //   console.log(show);
  //   setShow(show);
  // }
  return (
    <div>
      <h3>Name {show.name}</h3>
      <p>Type {show.type}</p>
      <p>Language {show.language}</p>
      <p>Genres {show.genres}</p>
      <p>Ended {show.ended}</p>
      <p>Premiered {show.premiered}</p>
      <p>OfficialSite {show.officialSite}</p>
      <p>summary {show.summary}</p>
    </div>
  )
}

export default ShowSummary
