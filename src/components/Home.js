import React, { useEffect } from 'react'

// class Home extends React.Component {
//   render() {
//     return (
//       <div>
//         <div>

//         </div>
//       </div>
//     )
//   }
// }
function Home ({ showData, fetch }) {
  useEffect(() => {
    fetch()
  }, [])
  return showData.loading ? (
    <h2>Loading</h2>
  ) : showData.error ? (
    <h2>{showData.error}</h2>
  ) : (
    <div>
      <h2>Shows List</h2>
      <div>
        {showData &&
          showData.shows &&
          showData.shows.map(s => <p>{s.name}</p>)}
      </div>
    </div>
  )
}
export default Home