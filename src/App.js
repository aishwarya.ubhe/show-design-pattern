import React from 'react';
import { BrowserRouter as Router, Switch,Route } from 'react-router-dom';
// import {createStore,applyMiddleware} from 'redux'
import {Provider } from 'react-redux'
import store from './duck/store'

import HomeContainer from './container/HomeContainer';
import Nav from './Nav'
import reducer from './duck/Home/reducer'
import actions from './duck/Home/actions'



function App() {
  return (
    <Provider store={store}>
      <div>
        <Router>
          <Nav />
          <Route path="/" component={HomeContainer} />
        </Router>
      </div>
    </Provider>
  );
}
export default App;
